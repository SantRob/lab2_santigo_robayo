public class Game {

  public String winner(String[] deckSteve, String[] deckJosh) {
    int scoreSteve = 0, scoreJosh = 0;

    String nivel = "23456789TJQKA";

    for (int i = 0; i < deckSteve.length; i++) {
      char cardSteve = deckSteve[i].charAt(0);
      char cardJosh = deckJosh[i].charAt(0);

      if (nivel.indexOf(cardSteve) > nivel.indexOf(cardJosh)) {
        scoreSteve++;
      } else if (nivel.indexOf(cardJosh) > nivel.indexOf(cardSteve)) {
        scoreJosh++;
      }
    }

    if (scoreSteve > scoreJosh) {
      return "Steve wins " + scoreSteve + " to " + scoreJosh;
    } else if (scoreJosh > scoreSteve) {
      return "Josh wins " + scoreJosh + " to " + scoreSteve;
    } else {
      return "Tie";
    }
  }
}
