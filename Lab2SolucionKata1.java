public class Kata {

  public static String solution(String str){
    return reverseString(str);
  }
  public static String reverseString(String input) {
    StringBuilder reversed = new StringBuilder();
    for (int i = input.length() - 1; i >= 0; i--) {
        reversed.append(input.charAt(i));
    }
    return reversed.toString();
  }

}
